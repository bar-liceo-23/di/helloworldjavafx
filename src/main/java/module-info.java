module com.liceo.des.bar.helloworldjavafx {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.liceo.des.bar.helloworldjavafx to javafx.fxml;
    exports com.liceo.des.bar.helloworldjavafx;
}