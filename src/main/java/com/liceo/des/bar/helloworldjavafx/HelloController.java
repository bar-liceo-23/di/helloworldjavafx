package com.liceo.des.bar.helloworldjavafx;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class HelloController {
    @FXML
    private Label welcomeText;
    @FXML
    private Label contadorLabel;
    @FXML
    private VBox vBoxMain;
    @FXML
    private Button helloButton;
    @FXML
    private Button botonCerrar;

    int contadorDeClicks = 0;
    boolean haiBotonDeSalir = false;

    @FXML
    protected void onHelloButtonClick() {
        System.out.println("Style: " + welcomeText.getStyle());
        contadorDeClicks++;
        welcomeText.setText("Pulsado botón " + contadorDeClicks + " veces.");
        contadorLabel.setText(Integer.toString(contadorDeClicks));
        System.out.println("Contador: " + contadorDeClicks);
        System.out.println(vBoxMain.getChildren());
        if (!haiBotonDeSalir) {
            botonCerrar.setVisible(true);
            this.haiBotonDeSalir = true;
        }

        if (contadorDeClicks > 20){
            // vermello
            welcomeText.setStyle("-fx-background-color: red;");
            if (contadorDeClicks > 30){
                //desactivamos
                helloButton.setDisable(true);
            }
        } else {
            if (contadorDeClicks > 10){
                //laranxa
                welcomeText.setStyle("-fx-background-color: orange;");
            }
        }
    }
    public void onBotonCerrarClick(){
        System.exit(0);
    }
}